CREATE DATABASE  IF NOT EXISTS `joinelderly` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `joinelderly`;
-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: joinelderly
-- ------------------------------------------------------
-- Server version	5.5.44-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL,
  `emisor` varchar(45) NOT NULL,
  `receptor` varchar(45) NOT NULL,
  `mensaje` varchar(100) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` time NOT NULL,
  `valoracion` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios`
--

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;
INSERT INTO `comentarios` VALUES (1,'lauramore','josegg85','Me ha gustado mucho la actividad que has realizado','10/06/2016','17:30:00',8),(2,'lauramore','miguelgh','Me hubiera gustado hacer la actividad durante mas tiempo','20/05/2016','19:08:00',7);
/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ofertas`
--

DROP TABLE IF EXISTS `ofertas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ofertas` (
  `id` int(11) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `fecha_inicio` varchar(100) NOT NULL,
  `fecha_final` varchar(100) NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_final` time NOT NULL,
  `lugar_inicio` varchar(45) NOT NULL,
  `lugar_final` varchar(45) NOT NULL,
  `asistentes` varchar(200) NOT NULL,
  `precio` float NOT NULL,
  `latitud` float NOT NULL,
  `longitud` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ofertas`
--

LOCK TABLES `ofertas` WRITE;
/*!40000 ALTER TABLE `ofertas` DISABLE KEYS */;
INSERT INTO `ofertas` VALUES (1,'josegg85','Excursion a Tierra Natura','20/05/2016','20/05/2016','09:00:00','19:00:00','Ontinyent','Ontinyent','Paco&Miguel',25,38.8175,-0.604407),(2,'miguelgh','Visita al museo botanico','10/06/2016','10/06/2016','10:00:00','13:30:00','Ontinyent','Ontinyent','Jose&Laura',10,38.8175,-0.614507),(3,'josegg85','Partida de Petanca','15/02/2016','15/02/2016','09:30:00','12:00:00','Ontinyent','Ontinyent','Jose&Laura',2,38.8119,-0.610749);
/*!40000 ALTER TABLE `ofertas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `usuario` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `apellidos` varchar(100) CHARACTER SET latin1 NOT NULL,
  `dni` varchar(9) CHARACTER SET latin1 NOT NULL,
  `password` varchar(200) CHARACTER SET latin1 NOT NULL,
  `date_birthday` varchar(45) CHARACTER SET latin1 NOT NULL,
  `tipo` varchar(45) CHARACTER SET latin1 NOT NULL,
  `bank` varchar(45) CHARACTER SET latin1 NOT NULL,
  `pais` varchar(100) CHARACTER SET latin1 NOT NULL,
  `provincia` varchar(100) CHARACTER SET latin1 NOT NULL,
  `poblacion` varchar(100) CHARACTER SET latin1 NOT NULL,
  `avatar` varchar(200) CHARACTER SET latin1 NOT NULL,
  `valoracion` float NOT NULL,
  `activado` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('158218454554356','joinelderly@gmail.com','JoinElderly','JE','','','','client','',' ',' ',' ','http://graph.facebook.com/158218454554356/picture',0,1,''),('4702104732','','JoinElderly','','','','','client','',' ',' ',' ','http://graph.facebook.com/4702104732/picture',0,1,''),('Raul','raulcolomer_revert@hotmail.com','Raul','Colomer','48607190W','$2y$10$GkUNfziurcesbp/ngH6vr.C5nDE4KYKWJNTaQpg0THgym7XeQ/.UO','21/05/1993','client','65895577889','ES','46','Bufali','http://www.gravatar.com/avatar/fb4b4fea17f9e0e3a91151dd14e8f538fb4b4fea17f9e0e3a91151dd14e8f538?s=400&d=identicon&r=g',0,1,'Chac929f846cd76bdc8f8f9714fc1203219'),('Vicent','vicent.albo@gmail.com','vicent','albert borrell','48607270J','$2y$10$r71.krqgflTdO7hLNOe2i.bqEbwlbMHa5D/qd1.tvBrTpntCDShfG','12/01/1995','client','1515424554',' ',' ',' ','http://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427ed41d8cd98f00b204e9800998ecf8427e?s=400&d=identicon&r=g',0,1,'Ver14a40272b4c4a9f052d3f72fc00d2f8a');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'joinelderly'
--

--
-- Dumping routines for database 'joinelderly'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-02-01 21:33:51
