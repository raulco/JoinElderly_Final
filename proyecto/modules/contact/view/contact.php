<br><br>

<script src="<?php echo CONTACT_LIB_PATH; ?>bootstrap-button.js"></script>
<script src="<?php echo CONTACT_LIB_PATH; ?>jquery.validate.min.js"></script>
<script src="<?php echo CONTACT_LIB_PATH; ?>jquery.validate.extended.js"></script>
<script src="<?php echo CONTACT_JS_PATH; ?>contact.js"></script>


<div class="container">
    <form id="contact_form" name="contact_form" class="form-contact">
        <br>
        <h2 class="form-contact-heading">Contacto</h2>

        <div class="control-group">
            <input type="text" id="inputName" name="inputName" placeholder="Nombre" class="input-block-level" dir="auto" maxlength="100">
        </div>
        <div class="control-group">
            <input type="text" id="inputEmail" name="inputEmail" placeholder="Email *" class="input-block-level" maxlength="100">
        </div>
        <div class="control-group">
            <label for="sel1">Tema de Consulta</label>
            <select class="form-control" id="inputSubject" name="inputSubject" title="Choose subject">
                <option value="">- Por favor, seleccione un tema de consulta -</option>
                <option value="actividad">Info relativa a alguna actividad</option>
                <option value="dpto">Contacta con nuestro dpto de actividades</option>
                <option value="trabaja">Trabaja con nosotros</option>
                <option value="sugerencias">Haznos sugerencias</option>
                <option value="reclamaciones">Atendemos tus reclamaciones</option>
                <option value="novedades">Te avisamos de nuestras novedades</option>
                <option value="diferente">Algo diferente</option>
            </select>
        </div>
        <div class="control-group">
            <textarea class="input-block-level" rows="4" name="inputMessage" placeholder="Introduzca aqui su mensaje *" style="max-width: 100%;" dir="auto"></textarea>
        </div>

        <input type="hidden" name="token" value="contact_form" />

        <input class="btn btn-primary" type="submit" name="submit" id="submitBtn" disabled="disabled" value="Enviar" />

        <img src="<?php echo CONTACT_IMG_PATH; ?>ajax-loader.gif" alt="ajax loader icon" class="ajaxLoader" /><br/><br/>

        <div id="resultMessage" style="display: none;"></div>
    </form>
</div> <!-- /container -->
<div class="container">
    <section class="contacto">
        <div class="6u 12u(3)">
            <br>
            <h4>O bien puedes ponerte en contacto con nosotros mediante: </h4>
            <br>
            <ul class="icons">
                <li><a href="https://mail.google.com/"><img src="<?php echo VIEW_MEDIA ?>gmail.png" class="gmail"></a><span class="correogmail">joinelderly@gmail.com</span></li><br><br>
                <li><a href="https://twitter.com/JoinElderly" class="icon rounded fa-twitter"><span class="label">Twitter</span></a><span class="redsocial">@JoinElderly</span></li><br><br>
                <li><a href="https://www.facebook.com/profile.php?id=100010986072513" class="icon rounded fa-facebook"><span class="label">Facebook</span></a><span class="redsocial">facebook.com/JoinElderly</span></li><br><br>
            </ul>
        </div>

    </section>
</div>