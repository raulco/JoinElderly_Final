  <?php   
    class controller_main { 
         
        function __construct() {
         
        }
         
        function begin() {
           
            loadView('modules/main/view/', 'main.php');

        }
        
        function terminos() {
            loadView('modules/main/view/', 'terminos.html');
        }
        
        function politica() {
            loadView('modules/main/view/', 'politica.html');
        }
        
        function aviso() {
            loadView('modules/main/view/', 'aviso.html');
        }
        
        function trabaja() {
            loadView('modules/main/view/', 'trabaja.html');
        }
    }