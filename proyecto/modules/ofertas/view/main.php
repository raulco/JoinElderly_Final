<!-- Main -->
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>

<script src="<?php echo OFERTAS_JS_PATH ?>geolocator.js"></script>

<section id="main" class="wrapper">
    <div class="container">

        <header class="major">
            <h2>Ofertas</h2>
            <p>Aquí puedes ver las ofertas disponibles en tu zona</p>
        </header>

        <div class="image fit">
            <div id='ubicacion'></div>
            <!-- Se escribe un mapa con la localizacion anterior-->
            <div id="demo"></div>
            <div id="mapholder"></div>
            <div class="ofertas"></div>
        </div>

    </div>
</section>
