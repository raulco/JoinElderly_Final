$(document).ready(function () {

    $("#logout").click(function () {
        logout();
    });

});

function logout(){

    destroyCookie("session");
    
    window.location.href = amigable("?module=main");
    
}
