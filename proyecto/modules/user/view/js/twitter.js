$(document).ready(function () {

    var url = window.location.href;

    url = url.split("&");
    if (url[2])
        if (url[2].substring(0, 11) == 'oauth_token') {
            url = amigable(url[2] + "&" + url[3]);
            url = url.split("/");

            var tw = getCookie("tw");
            tw = tw.split("|");
            data = {'oauth_token': url[1], 'oauth_verifier': url[2], 'token': tw[0], 'token_secret': tw[1]};
            data = JSON.stringify(data);
            $.post(amigable('?module=user&function=twitter_signin'), {twitter: data},
            function (response) {
                if (response.success) {


                    final_login(response);



                } else {
                    if (response.error == 503)
                        window.location.href = amigable("?module=main&fn=begin&param=503");
                    else if (response.redirect)
                        window.location.href = response.redirect;

                }
            }, "json").fail(function (xhr, textStatus, errorThrown) {
                console.log(xhr.responseText);
                if (xhr.status === 0) {
                    alert('Not connect: Verify Network.');
                } else if (xhr.status === 404) {
                    alert('Requested page not found [404]');
                } else if (xhr.status === 500) {
                    alert('Internal Server Error [500].');
                } else if (textStatus === 'parsererror') {
                    alert('Requested JSON parse failed.');
                } else if (textStatus === 'timeout') {
                    alert('Time out error.');
                } else if (textStatus === 'abort') {
                    alert('Ajax request aborted.');
                } else {
                    alert('Uncaught Error: ' + xhr.responseText);
                }
            });
        }


});

function loginTw() {

    $.post(amigable('?module=user&function=twitter_signin'), {user: {datos_social: true}},
    function (response) {

        if (response.success) {

            setCookie("tw", response.tw, 1);

            window.location.href = response.url;




        } else {
            if (response.error == 503)
                window.location.href = amigable("?module=main&fn=begin&param=503");
            else if (response.redirect)
                window.location.href = response.redirect;

        }
    }, "json").fail(function (xhr, textStatus, errorThrown) {
        console.log(xhr.responseText);
        if (xhr.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (xhr.status === 404) {
            alert('Requested page not found [404]');
        } else if (xhr.status === 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + xhr.responseText);
        }
    });

}

function final_login(response) {
    var data = {'nombre': response.screen_name, 'id': response.user_id, 'twitter': true};
    data=JSON.stringify(data);
    $.post(amigable('?module=user&function=social_signin'), {user: data},
    function (response) {
        if (!response.error) {
            setCookie("session", response[0]['usuario'] + "|" + response[0]['avatar'] + "|" + response[0]['tipo'] + "|" + response[0]['nombre'], 1);
            window.location.href = amigable("?module=main");
        } else {
            if (response.error == 503)
                window.location.href = amigable("?module=main&fn=begin&param=503");
            else if (response.redirect)
                window.location.href = response.redirect;

        }
    }, "json").fail(function (xhr, textStatus, errorThrown) {
        console.log(xhr.responseText);
        if (xhr.status === 0) {
            alert('Not connect: Verify Network.');
        } else if (xhr.status === 404) {
            alert('Requested page not found [404]');
        } else if (xhr.status === 500) {
            alert('Internal Server Error [500].');
        } else if (textStatus === 'parsererror') {
            alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            alert('Time out error.');
        } else if (textStatus === 'abort') {
            alert('Ajax request aborted.');
        } else {
            alert('Uncaught Error: ' + xhr.responseText);
        }
    });
}