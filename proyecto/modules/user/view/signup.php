
<br /> <br />
<script src="<?php echo USER_JS_PATH . "signup.js" ?>"></script>
<div class="container">
    <form id="signup_form" name="signup_form" class="form-contact">
        <br />
        <div class="form-title"><h2 class="form-contact-heading">Registro</h2></div>
        <div class="form-row">
            <div class="control-group">
                <p>Usuario: <input type="text" id="inputUser" name="inputUser" placeholder="Usuario" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
            <div class="control-group">
                <p>Email: <input type="text" id="inputEmail" name="inputEmail" placeholder="Email" class="input-block-level" maxlength="100">
                </p>
            </div>
        </div>
        <div class="form-row">
            <div class="control-group">
                <p>Nombre: <input type="text" id="inputName" name="inputName" placeholder="Nombre" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
            <div class="control-group">
                <p>Apellidos: <input type="text" id="inputSurn" name="inputSurn" placeholder="Apellidos" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
        </div>
        <div class="form-row">
            <div class="control-group">
                <p>Contraseña: <input type="password" id="inputPass" name="inputPass" placeholder="Contraseña" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
            <div class="control-group">
                <p>Repita la contraseña: <input type="password" id="inputPass2" name="inputPass2" placeholder="Contraseña" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
        </div>
        <div class="form-row">
            <div class='control-group'>
                <p>Fecha de Nacimiento: <input type="text" id="inputBirth" name="inputBirth" class="form-control" dir="auto" maxlength="100"></p>
            </div>
            <div class="control-group">
                <p>Tipo de usuario: 
                    <select class="form-control" id="inputType" name="inputType">
                        <option value="client">Cliente</option>
                        <option value="worker">Ofertante</option>
                    </select></p>
            </div>
        </div>
        <div class="form-row">
            <div class="control-group">
                <p>Nº cuenta bancaria: <input type="text" id="inputBank" name="inputBank" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
            <div class="control-group">
                <p>DNI: <input type="text" id="inputDni" name="inputDni" placeholder="DNI" class="input-block-level" dir="auto" maxlength="100">
                </p>
            </div>
            <input type="hidden" name="token" value="signup_form" />


        </div>
        <div class="form-row">
            <div><br /><input class="btn btn-primary" type="button" name="submit" id="submitBtn"  value="Enviar" />
            </div>
        </div>
        <div id="resultMessage" style="display: none;"></div>
    </form>
</div> <!-- /container -->