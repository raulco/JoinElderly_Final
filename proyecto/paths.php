<?php

//PROYECTO
define('PROJECT', '/JoinElderly_project/proyecto');

//SITE_ROOT
define('SITE_ROOT', $_SERVER['DOCUMENT_ROOT'] . PROJECT);

//SITE_PATH
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . PROJECT);

//CSS
define('CSS_PATH', SITE_PATH . '/view/css/');

//JS
define('JS_PATH', SITE_PATH . '/view/js/');

//view
define('VIEW_PATH', SITE_PATH . '/view/');
define('VIEW_PATH_INC', SITE_ROOT . '/view/includes/');

//log
define('USER_LOG_DIR', SITE_ROOT . '/log/user/Site_User_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . '/log/general/Site_General_errors.log');

//production
define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . '/model/');

//modules
define('MODULES_PATH', SITE_ROOT . '/modules/');

//resources
define('RESOURCES', SITE_ROOT . '/resources/');

//media
define('MEDIA_ROOT', SITE_ROOT . '/media/');
define('MEDIA_PATH', SITE_PATH . '/media/');

//ViewMedia
define('VIEW_MEDIA', SITE_PATH . '/view/media/');

//utils
define('UTILS', SITE_ROOT . '/utils/');

//Activacio URL amigables
define('URL_AMIGABLES', TRUE);

//libs
define('LIBS', SITE_ROOT . '/libs/');

//classes
define('CLASSES', SITE_ROOT . '/classes/');

//img JoinElderly
define('IMG_JOINELDERLY', SITE_ROOT . '/view/media/Logo.png');

//model contact
define('CONTACT_JS_PATH', SITE_PATH . '/modules/contact/view/js/');
define('CONTACT_CSS_PATH', SITE_PATH . '/modules/contact/view/css/');
define('CONTACT_LIB_PATH', SITE_PATH . '/modules/contact/view/lib/');
define('CONTACT_IMG_PATH', SITE_PATH . '/modules/contact/view/img/');

//model users
define('USER_JS_PATH', SITE_PATH . '/modules/user/view/js/');
define('USER_CSS_PATH', SITE_PATH . '/modules/user/view/css/');
define('UTILS_USER', SITE_ROOT . '/modules/user/utils/');
define('MODEL_USER', SITE_ROOT . '/modules/user/model/model/');

//model ofertas
define('OFERTAS_CSS_PATH', SITE_PATH . '/modules/ofertas/view/css/');
define('OFERTAS_JS_PATH', SITE_PATH . '/modules/ofertas/view/js/');
define('MODEL_OFERTAS', SITE_ROOT . '/modules/ofertas/model/model/');

//model main
define('MAIN_IMG_PATH', SITE_PATH . '/modules/main/view/media/');
define('MAIN_CSS', SITE_PATH . '/modules/main/view/css/');


