<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>
            JoinElderly: <?php
            if (!isset($_GET['module'])) {
                echo "JoinElderly";
            } else {
                echo $_GET['module'];
            }
            ?>
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--Favicon-->
        <link rel="shortcut icon" type="image/png" href="<?php echo VIEW_MEDIA ?>favicon.ico">
        <!--JS-->
        <script type="text/javascript" src="<?php echo JS_PATH ?>jquery.min.js"></script>
        <script src="<?php echo JS_PATH ?>main.js"></script>
        <script type="text/javascript" src="<?php echo JS_PATH ?>skel.min.js"></script>
        <script type="text/javascript" src="<?php echo JS_PATH ?>skel-layers.min.js"></script>
        <script type="text/javascript" src="<?php echo JS_PATH ?>init.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="<?php echo PROJECT . '/' ?>view/js/cookies.js"></script>
        <script type="text/javascript" src="<?php echo USER_JS_PATH ?>init.js"></script>

        <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
        <script type="text/javascript" src="<?php echo USER_JS_PATH ?>twitter.js"></script>

        <!--CSS -->
        <link href="<?php echo CONTACT_CSS_PATH; ?>bootstrap.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>skel.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>style.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>style-xlarge.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo USER_CSS_PATH ?>main.css" type="text/css"/>
        <link href="<?php echo CONTACT_CSS_PATH; ?>custom.css" rel="stylesheet"/>
        <link href="<?php echo OFERTAS_CSS_PATH; ?>main.css" rel="stylesheet"/>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
        <link href="<?php echo MAIN_CSS; ?>footercss.css" rel="stylesheet" type="text/css">
    </head>

